(function ($, Drupal, drupalSettings) {
	Drupal.behaviors.APISireneDataFrBehavior = {
	    attach: function (context, settings) {
			$(document).ready(function() {
				var field_address_id = drupalSettings.api_adresses_open_data_france.field_address_id;
				var field_postal_code_id = drupalSettings.api_adresses_open_data_france.field_postal_code_id;
				var field_city_id = drupalSettings.api_adresses_open_data_france.field_city_id;
				$('#'+field_address_id).autocomplete({
					source : function(requete, reponse){ // les deux arguments représentent les données nécessaires au plugin
					$.ajax({
							url : Drupal.url('api_adresses_open_data_france/address-autocomplete'), // on appelle le script JSON
							dataType : 'json', // on spécifie bien que le type de données est en JSON
							type: "POST",
							data : {
								//variable envoyé avec la requête vers le serveur
								name_startsWith : $('#'+field_address_id).val(), // on donne la chaîne de caractère tapée dans le champ de recherche 
							},
							success : function(donnee){
								//donnee est la variable reçu du serveur avec les résultats
								reponse($.map(donnee, function(objet){
									// on retourne cette forme de suggestion avec ces datas
									return {
										'label':objet.properties.label,
										'value':objet.properties.name,
										'postcode':objet.properties.postcode,
										'id':objet.properties.id, 
										'city':objet.properties.city,
									}; 
								}));
							}
						});
					}
				});

				$('#'+field_address_id).on( "autocompleteselect", function( event, ui ) {
					var postcode = ui.item.postcode;
					var city = ui.item.city;
					$('#'+field_postal_code_id).val(postcode);
					$('#'+field_city_id).val(city);
				});

			});
		}
	};
})(jQuery, Drupal, drupalSettings);