<?php

namespace Drupal\api_adresses_open_data_france\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Component\Serialization\JSON;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\api_adresses_open_data_france\GeoAPIGouvFrManager;

class AddressAjaxController extends ControllerBase {
  
  //function for autocomplete
  public function address_autocomplete(Request $request){
    $return = []; //our variable to fill with data to return to autocomplete result
    
    $search_string = \Drupal::request()->request->get('name_startsWith');
    $type = "housenumber"; // can be housenumber, street, locality or municipality.

    $geoApi = new GeoAPIGouvFrManager();
    $return = $geoApi->searchPlace($geoApi->buildOptionsSearchAutocomplete($search_string, $type, 10));

    return new JsonResponse(json_encode($return['features']), 200, [], true);
  }
  
  //search function
  public function address_autocomplete_search(){
    //same logic as first function
    $return = [];
    return new JsonResponse(json_encode($return), 200, [], true);
  }
  
}