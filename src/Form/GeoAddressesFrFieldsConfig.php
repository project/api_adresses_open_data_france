<?php

namespace Drupal\api_adresses_open_data_france\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure API Sirene Open Data fields settings.
 */
class GeoAddressesFrFieldsConfig extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'api_adresses_open_data_france.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'api_adresses_open_data_france_config_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['field_address_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Address ID'),
      '#default_value' => $config->get('field_address_id'),
      '#description' => $this->t('Enter your form field\'s HTML ID without the hashtag sign (#)'),
    ];  

    $form['field_postal_code_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Field Postal Code ID'),
        '#default_value' => $config->get('field_postal_code_id'),
        '#description' => $this->t('Enter your form field\'s HTML ID without the hashtag sign (#)'),
    ];

    $form['field_city_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Field City ID'),
        '#default_value' => $config->get('field_city_id'),
        '#description' => $this->t('Enter your form field\'s HTML ID without the hashtag sign (#)'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('field_address_id', $form_state->getValue('field_address_id'))
      ->set('field_postal_code_id', $form_state->getValue('field_postal_code_id'))
      ->set('field_city_id', $form_state->getValue('field_city_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
